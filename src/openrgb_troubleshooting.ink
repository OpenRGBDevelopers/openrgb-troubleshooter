# author: OpenRGB 
# title: OpenRGB Troubleshooting

VAR platform = ""
VAR language = ""
-> Start

== Start
{ language == "": -> Language }
{ platform == "": -> Platform }
{ language == "Français": -> Francais }  //Ink does not like non ascii characters in links
{ language == "Deutsch":  -> Deutsch  }
{ language == "Español":  -> Espanol  }  //Ink does not like non ascii characters in links
{ language == "Русский":  -> Russian  }  //Ink does not like non ascii characters in links
-> English  //Default stitch to display 

= English
{ | You selected to run the troubleshooter in English. } # CLASS:selected
You are currently using {platform} # CLASS:selected
-> common

= Francais
{ | Vous avez choisi d'exécuter l'utilitaire de résolution des problèmes en français.} # CLASS:selected
Vous utilisez actuellement {platform} # CLASS:selected
-> common

= Deutsch
{ | Sie haben für die Fehlerbehebung die Sprache Deutsch ausgewählt.} # CLASS:selected
Sie verwenden derzeit {platform} # CLASS:selected
-> common

= Espanol
{ | Has seleccionado correr el programa de resolución de problemas en Español.} # CLASS:selected
Actualmente estás usando {platform} # CLASS:selected
-> common

= Russian
{ | Вы запустили помощник на русском.} # CLASS:selected
Вы сейчас используете {platform} # CLASS:selected
-> common

- (common)
{ platform == "Linux": -> Linux }
{ platform == "MacOS": -> MacOS }
{ platform == "Windows": -> Windows }



== Language
//Languages have been added for demonstartion purposes for now. Will need translators.
Please select a language to begin troubleshooting # BACKGROUND:green.webp
    *   [English]
        ~ language = "English"
        Excellent, let us begin!
    *   [Français]
        ~ language = "Français"
        Excellent, commençons!
    *   [Deutsch]
        ~ language = "Deutsch"
        Ausgezeichnet, fangen wir an!
    *   [Español]
        ~ language = "Español"
        ¡Excelente, comencemos!
    *   [Русский]
        ~ language = "Русский"
        Отлично, давайте начнём!
    -   -> Start    //Jump to "Start" to continue



== Platform
{ language == "Français": -> Francais }  //Ink does not like non ascii characters in links
{ language == "Deutsch":  -> Deutsch  }
{ language == "Español":  -> Espanol  }  //Ink does not like non ascii characters in links
{ language == "Русский":  -> Russian  }  //Ink does not like non ascii characters in links
-> English  //Default stitch to display 

= English
Ok, best to start by establishing which platform you are on. 
-> common

= Francais
Ok, alors commençons par établir sur quelle plateforme vous vous trouvez.
-> common

= Deutsch
Ok, wählen sie aus, auf welcher Platform Sie sind. 
-> common

= Espanol
Bien, comencemos por establecer en qué plataforma te encuentras.
-> common

= Russian
Хорошо, для начала укажите используемую платформу.
-> common

- (common)
    *   [Linux] 
        ~ platform = "Linux"    
    *   [MacOS]
        ~ platform = "MacOS"
    *   [Windows]
        ~ platform = "Windows"
    -   -> Start
  
  
        
== Linux    //This ensures that the user only has to enter the platform once

-> precheck

= precheck
-> Troubleshooter_unable_to_assist

== MacOS    //This ensures that the user only has to enter the platform once

-> precheck

= precheck
-> Troubleshooter_unable_to_assist

== Windows
{ not Windows_admincheck: -> Windows_admincheck }
-> Windows_devices



== Windows_admincheck
{ language == "Français": -> Francais }  //Ink does not like non ascii characters in links
{ language == "Deutsch":  -> Deutsch  }
{ language == "Español":  -> Espanol  }  //Ink does not like non ascii characters in links
{ language == "Русский":  -> Russian  }  //Ink does not like non ascii characters in links
-> English  //Default stitch to display 

= English
    Let me check that you have run OpenRGB at least once as Administrator?
            *   Yes, I have run it as Administrator. # CLASS:selected
                -> Windows_devices
            *   No, but I will go and do that right now! # CLASS:selected
            OpenRGB needs to install a small helper library to enable access to i2c and SMBus devices.
            This step requires elevated privlidges to complete.
            Did running as Administrator resolve the issue?
                *   *   Yes, thanks very much! # CLASS:selected
                Brilliant, I do love those easy fixes. Glad we could resolve that issue for you-> Salutation
                *   *   No, Sadly it's something else. # CLASS:selected
                    -> Windows_devices

= Francais
    Avez-vous lancé OpenRGB en tant qu'administrateur ?
            *   Oui je l'ai lancé en tant qu'administrateur. # CLASS:selected
                -> Windows_devices
            *   Non, je le fais maintenant ! # CLASS:selected
            OpenRGB a besoin d'installer une bibliothèque pour autoriser l'accès aux périphériques i2c et SMBus.
            Cette étape nécessite les droits administrateurs pour fonctionner.
            Lancer OpenRGB en tant qu'administrateur a-t'il résolu le problème?
                *   *   Oui, merci beaucoup ! # CLASS:selected
                Excellent, j'adore ce genre de résolutions faciles. Contents d'avoir pu vous aider. -> Salutation
                *   *   Non, malheuresemnt cela doit être autre chose. # CLASS:selected
                    -> Windows_devices
= Deutsch
    Haben Sie OpenRGB mindestens ein Mal als Administrator ausgeführt?
            *   Ja, das habe ich. # CLASS:selected
                -> Windows_devices
            *   Nein, ich werde dies nun tun! # CLASS:selected
            OpenRGB muss eine kleine Bibliotek installieren, um auf i2c und SMBus Geräte zuzugreifen.
            Dazu benötigt OpenRGB Administratorrechte.
            Haben die Administratorrechte geholfen?
                *   *   Ja, vielen Dank! # CLASS:selected
                Perfekt. Einfach behoben. Wir sind froh das wir Ihnen helfen konnte.-> Salutation
                *   *   Nein, es ist leider noch etwas anderes. # CLASS:selected
                    -> Windows_devices
                    
= Espanol
    ¿Has ejecutado OpenRGB como administrador al menos una vez?
            *   Sí, lo he ejectuado como administrador. # CLASS:selected
                -> Windows_devices
            *   ¡No, pero voy a hacerlo ahora mismo! # CLASS:selected
            OpenRGB necesita instalar una pequeña librería para acceder a los dispositivos i2c y SMBus.
            Este paso necesita privilegios de administrador.
            ¿Se ha solucionado el problema?
                *   *   Sí, ¡muchas gracias! # CLASS:selected
                Excelente, me alegro de que se haya podido resolver tan facilmente -> Salutation
                *   *   No, debe ser otra cosa. # CLASS:selected
                    -> Windows_devices

= Russian
    Вы ведь уже запускали OpenRGB от имени Администратора хотя бы раз?
            *   Да, запускал. # CLASS:selected
                -> Windows_devices
            *   Ещё нет, сейчас сделаю! # CLASS:selected
            OpenRGB нужно установить небольшую библиотеку, чтобы получить доступ к устройствам i2c и SMBus.
            А для этого понадобятся привилегии Администратора.
            Помог ли запуск от имени Администратора?
                *   *   Да, спасибо! # CLASS:selected
                Отлично! люблю, когда проблемы решаются вот так запросто. Рад был помочь!-> Salutation
                *   *   Увы, дело, похоже, не в этом. # CLASS:selected
                    -> Windows_devices


                    
== Windows_devices
{ language == "Français": -> Francais }  //Ink does not like non ascii characters in links
{ language == "Deutsch":  -> Deutsch  }
{ language == "Español":  -> Espanol  }  //Ink does not like non ascii characters in links
{ language == "Русский":  -> Russian  }  //Ink does not like non ascii characters in links
-> English  //Default stitch to display 

= English
    Is there an issue with one of your devices?
        +   Yes, the device is not appearing in the devices list. # CLASS:selected
            -> Windows_device_not_present
        +   Yes, the device is present in the list but is not responding to the user interface. # CLASS:selected
            -> Windows_device_not_responding
        +   No, I have another issue. # CLASS:selected
            -> Troubleshooter_unable_to_assist

= Francais
    Il y a t'il un problème avec l'un de vos périphériques ?
        +   Oui, il n'apparaît pas dans la liste des périphériques. # CLASS:selected
            -> Windows_device_not_present
        +   Oui, il apparaît dans la liste des périphériques mais l'interface ne perment pas de le controller. # CLASS:selected
            -> Windows_device_not_responding
        +   Non, j'ai un autre problème. # CLASS:selected
            -> Troubleshooter_unable_to_assist
= Deutsch
    Gibt es ein Problem mit einer Ihrer Geräte?
        +   Ja, das Gerät taucht nicht im Programm auf. # CLASS:selected
            -> Windows_device_not_present
        +   Ja, das Gerät taucht auf, reagiert jedoch nicht auf die Benutzeroberfläche. # CLASS:selected
            -> Windows_device_not_responding
        +   Nein, es ist ein anderes Problem. # CLASS:selected
            -> Troubleshooter_unable_to_assist

= Espanol
    ¿Hay algún problema con alguno de tus dispositivos?
        +   Sí, el dispositivo no aparece en la lista. # CLASS:selected
            -> Windows_device_not_present
        +   Sí, el dispositivo aparece, pero no lo puedo controlar con la interficie gráfica. # CLASS:selected
            -> Windows_device_not_responding
        +   No, tengo un problema diferente. # CLASS:selected
            -> Troubleshooter_unable_to_assist   

= Russian
    Возникла проблема с одним из Ваших устройств?
        +   Да, устройство не отображается в списке. # CLASS:selected
            -> Windows_device_not_present
        +   Да, устройство отображается в списке, но не отвечает на команды интерфейса. # CLASS:selected
            -> Windows_device_not_responding
        +   Нет, у меня проблема другого рода. # CLASS:selected
            -> Troubleshooter_unable_to_assist



== Windows_device_supported_not_detected
{ language == "Français": -> Francais }  //Ink does not like non ascii characters in links
{ language == "Deutsch":  -> Deutsch  }
{ language == "Español":  -> Espanol  }  //Ink does not like non ascii characters in links
{ language == "Русский":  -> Russian  }  //Ink does not like non ascii characters in links
-> English  //Default stitch to display 

= English
    Do you know what type of device is having the issue. Put another way, do you know how the  undetected device is connected to the computer?
        +   The device is USB. # CLASS:selected
            -> Troubleshooter_unable_to_assist
        +   It's my graphics card (GPU) # CLASS:selected
            -> Troubleshooter_unable_to_assist
        +   It's the fans / light strip that's plugged into the Motherboard # CLASS:selected
            -> Troubleshooter_unable_to_assist
        +   My RAM is missing! # CLASS:selected
            -> Windows_missing_RAM
        +   I have no idea how the device connects to my PC. # CLASS:selected
            That is OK however it sounds like you need someone to chat to directly to help you solve that.
            -> Troubleshooter_help_links -> Salutation

= Francais
    Savez-vous quel type de périphérique a un problème ? Plus précisément, savez-vous quel est le type de connexion du périphérique ?
        +   C'est un périphérique USB. # CLASS:selected
            -> Troubleshooter_unable_to_assist
        +   C'est ma carte graphique (GPU) # CLASS:selected
            -> Troubleshooter_unable_to_assist
        +   Ce sont les ventilateurs / barres de LEDs  branchées à la carte mère # CLASS:selected
            -> Troubleshooter_unable_to_assist
        +   Mes barettes de RAM sont manquantes ! # CLASS:selected
            -> Windows_missing_RAM
        +   Je ne sais pas quel est le type de connexion. # CLASS:selected
            Pas de problème, le mieux est sûrement de venir discuter directement avec nous pour vous aider à résoudre votre problème.
            -> Troubleshooter_help_links -> Salutation

= Deutsch
    Wissen Sie, was für ein Gerätetyp dieses Gerät ist bzw. wissen Sie, wie dieses angeschlossen ist?
        +   Das Gerät wird über USB angeschlossen. # CLASS:selected
            -> Troubleshooter_unable_to_assist
        +   Es ist die Grafikkarte. (GPU) # CLASS:selected
            -> Troubleshooter_unable_to_assist
        +   Es sind die Lüfter / LEDs und werden an das Motherboar.d angeschlossen # CLASS:selected
            -> Troubleshooter_unable_to_assist
        +   Der Arbeitsspeicher taucht nicht in der Liste auf! # CLASS:selected
            -> Windows_missing_RAM
        +   Sie wissen nicht, wie dieses Gerät angeschlossen wird.  # CLASS:selected
            Sie können sich mit anderen auseinandersetzen, um dies herauszufinden.
            -> Troubleshooter_help_links -> Salutation

= Espanol
¿Sabes qué tipo de dispositivo está causando el problema? ¿Más precisamente, sabes cómo está conectado al ordenador?
        +   Es un dispositivo USB. # CLASS:selected
            -> Troubleshooter_unable_to_assist
        +   Es una tarjeta gráfica (GPU). # CLASS:selected
            -> Troubleshooter_unable_to_assist
        +   Son los ventiladores / tira de leds conectados a la placa base.# CLASS:selected
            -> Troubleshooter_unable_to_assist
        +   Son mis módulos de memoria. # CLASS:selected
            -> Windows_missing_RAM
        +   No sé qué tipo de conexión usa. # CLASS:selected
            No hay problema, aunque quizá necesitas hablar con alguien directamente para resolver el problema. 
            -> Troubleshooter_help_links -> Salutation

= Russian
    Знаете ли вы, к какой категории можно отнести это устройство? Иными словами, знаете ли вы, как это устройство подключается к компьютеру?
        +   Это USB-устройства. # CLASS:selected
            -> Troubleshooter_unable_to_assist
        +   Это видеокарта (GPU). # CLASS:selected
            -> Troubleshooter_unable_to_assist
        +   Это вентиляторы / светодиодная лента, подключенные к материнской плате. # CLASS:selected
            -> Troubleshooter_unable_to_assist
        +   Это оперативная память (RAM)! # CLASS:selected
            -> Windows_missing_RAM
        +   Без понятия. #CLASS:selected
            Это нормально, но похоже, что вам нужен кто-то, с кем можно поговорить напрямую, чтобы помочь вам решить эту проблему. 
            -> Troubleshooter_help_links -> Salutation



== Windows_device_not_responding
{ language == "Français": -> Francais }  //Ink does not like non ascii characters in links
{ language == "Deutsch":  -> Deutsch  }
{ language == "Español":  -> Espanol  }  //Ink does not like non ascii characters in links
{ language == "Русский":  -> Russian  }  //Ink does not like non ascii characters in links
-> English  //Default stitch to display 

= English
    Do you know what type of device is having the issue. Put another way, do you know how the  non-responsive device is connected to the computer?
        +   The device is USB. # CLASS:selected
            -> Troubleshooter_unable_to_assist
        +   It's my graphics card (GPU) # CLASS:selected
            -> Troubleshooter_unable_to_assist
        +   It's the fans / light strip that's plugged into the Motherbord # CLASS:selected
            -> Troubleshooter_unable_to_assist
        +   I have no idea how the device connects to my PC. # CLASS:selected
            That is OK however it sounds like you need someone to chat to directly to help you solve that.
            -> Troubleshooter_help_links -> Salutation

= Francais
    Savez-vous quel type de périphérique a un problème ? Plus précisément, savez-vous quel est le type de connexion du périphérique ?
        +   C'est un périphérique USB. # CLASS:selected
            -> Troubleshooter_unable_to_assist
        +   C'est ma carte graphique (GPU) # CLASS:selected
            -> Troubleshooter_unable_to_assist
        +   Ce sont les ventilateurs / barres de LEDs  branchées à la carte mère # CLASS:selected
            -> Troubleshooter_unable_to_assist
        +   Je ne sais pas quel est le type de connexion. # CLASS:selected
            Pas de problème, le mieux est sûrement de venir discuter directement avec nous pour vous aider à résoudre votre problème.
            -> Troubleshooter_help_links -> Salutation

= Deutsch
    Wissen Sie, was für ein Gerätetyp dieses Gerät ist bzw. wissen Sie, wie dieses angeschlossen ist?
        +   Das Gerät wird über USB angeschlossen. # CLASS:selected
            -> Troubleshooter_unable_to_assist
        +   Es ist die Grafikkarte. (GPU) # CLASS:selected
            -> Troubleshooter_unable_to_assist
        +   Es sind die Lüfter / LEDs und werden an das Motherboar.d angeschlossen # CLASS:selected
            -> Troubleshooter_unable_to_assist
        +   Sie wissen nicht, wie dieses Gerät angeschlossen wird.  # CLASS:selected
            Sie können sich mit anderen auseinandersetzen, um dies herauszufinden.
            -> Troubleshooter_help_links -> Salutation

= Espanol
¿Sabes cuál es el dispositivo que no responde? ¿Dicho de otro modo, de qué forma está conectado al ordenador?
        +   Es un dispositivo USB. # CLASS:selected
            -> Troubleshooter_unable_to_assist
        +   Es una tarjeta gráfica (GPU). # CLASS:selected
            -> Troubleshooter_unable_to_assist
        +   Son los ventiladores / tira de leds conectados a la placa base.# CLASS:selected
            -> Troubleshooter_unable_to_assist
        +   No sé qué tipo de conexión usa. # CLASS:selected
            No hay problema, aunque quizá necesitas hablar con alguien directamente para resolver el problema. 
            -> Troubleshooter_help_links -> Salutation
            
= Russian
Знаете ли вы, к какой категории можно отнести это устройство? Иными словами, знаете ли вы, как это устройство подключается к компьютеру?
        +   Это USB-устройство. # CLASS:selected
            -> Troubleshooter_unable_to_assist
        +   Это видеокарта (GPU). # CLASS:selected
            -> Troubleshooter_unable_to_assist
        +   Это вентиляторы / светодиодная лента, подключенные к материнской плате. # CLASS:selected
            -> Troubleshooter_unable_to_assist
        +   Без понятия. #CLASS:selected
            Бывает, но похоже, чтобы решить эту проблему, вам понадобится обсудить это с кем-то напрямую.
            -> Troubleshooter_help_links -> Salutation



== Windows_missing_RAM
{ language == "Français": -> Francais }  //Ink does not like non ascii characters in links
{ language == "Deutsch":  -> Deutsch  }
{ language == "Español":  -> Espanol  }  //Ink does not like non ascii characters in links
{ language == "Русский":  -> Russian  }  //Ink does not like non ascii characters in links
-> English  //Default stitch to display 

= English
    A regular Bo-Peep here! This is a common issue. If you've installed the helper library for RAM as you have mentioned then it's most likely that there is other software blocking access to the memory.
    If you have not yet checked to see if the OEM app for your RAM is running it's quite possible that it is running in the background. Open task manager and make sure that it is closed.
    Also make sure to check services by right clicking start, go to services and ensure that iCue / Synapse / GHub have been stopped and disabled.
        +   Brilliant! Thanks, that has fixed it for me. #CLASS:selected
            Awesome, it is an easy thing to overlook.
            ->Salutation
        +   I'm not sure how to do that or I tried but I don't think it worked #CLASS:selected
            No problem with that however it looks like you need a little more hand holding than I can offer you here.
            -> Troubleshooter_help_links -> Salutation        
        +   No tried that. Is it something else? #CLASS:selected
            Quite likely do you have Vanguard (Valorant), Faceit or HWMontior installed?
            +   +   Yes I do! #CLASS:selected
                -> Windows_vanguard_faceit_HWMonitor
            +   +   No, none of those installed. #CLASS:selected
                Then sadly you have discovered my only weakness and I can not assist you further.
                -> Troubleshooter_help_links -> Salutation
            +   +   Never heard of them what are they? #CLASS:selected
                HWMonitor does what it says on the tin. It monitors your hardware. It has to get up close and personal in order to do that.
                Vanguard is Riot's anti-cheat application which installs itself as a kernel driver.
                The Faceit esports platform is also bundled with their anti cheat software.
                If I was writing a report for the software it would include the phrases "Is disruptive in class" and "does not play well with others"
                Having said that it appears that I can not assist any further.
                -> Troubleshooter_help_links -> Salutation

= Francais
    C'est un problème courant. Si vous avez installé la bibliothèque d'aide pour la RAM comme mentionné, il est fort probable qu'un autre logiciel bloque l'accès à la mémoire.
    Si vous n'avez pas encore vérifié si l'application officielle pour votre RAM fonctionne, il est fort possible qu'elle s'exécute en arrière-plan. Ouvrez le gestionnaire de tâches et assurez-vous qu'elle n'est pas lancée. 
    Assurez-vous également de vérifier les services en cliquant avec le bouton droit sur Démarrer, accédez aux services et assurez-vous que iCue / Synapse / GHub ont été arrêtés et désactivés.
        +   Excellent ! Merci, c'est résolu. #CLASS:selected
            Super, c'est quelque-chose qu'on oublie facilement.
            ->Salutation
        +   Je ne sais pas comment faire cela ou j'ai essayé mais je ne pense pas que cela ait fonctionné. #CLASS:selected
            Pas de panique, cependant cet assistant ne pourra pas vous aider d'avantage.
            -> Troubleshooter_help_links -> Salutation        
        +   Non, j'ai déjà essayé. serait-ce autre chose ? #CLASS:selected
            Il se peut que vous ayez Vanguard (Valorant), Faceit ou HWMontior installé ?
            +   +   Oui en effet ! #CLASS:selected
                -> Windows_vanguard_faceit_HWMonitor
            +   +   Non, aucun de ces programmes sont installés. #CLASS:selected
                Alors, malheureusement, vous avez découvert ma seule faiblesse et je ne peux pas vous aider davantage.
                -> Troubleshooter_help_links -> Salutation
            +   +   Je n'ai jamais entendu parler de cela, qu'est-ce que c'est ? #CLASS:selected
                HWMonitor est un programme de surveillance des périphériques (fréquences, températures, etc...), pour cela il doit communiquer avec les périphériques.
                Vanguard est l'application anti-triche de Riot qui s'installe en tant que pilote du noyau.
                La plate-forme e-sport Faceit est également fournie avec leur logiciel anti-triche.
                Nous considérons ces programmes comme pertubateurs.
                Cela dit, il semble que je ne peux plus vous aider.
                -> Troubleshooter_help_links -> Salutation

= Deutsch
    Dies kommt häufig vor. Wenn diese Bibliotek bereits installiert ist kann es sein, dass ein anderes Program diese blockiert.
    Falls Sie es noch nicht überprüft haben kann es sein, dass die offizielle Software für Ihr Gerät im Hintergrund noch läuft. Öffnen Sie den Task Manager und beenden Sie es von dort. 
    Hier können Sie auch direkt die Hintergrundprozesse von den offiziellen Programmen wie ICUE / Synapse / GHub beenden. 
        +   Perfekt. Dies hat das Problem behoben. #CLASS:selected
            Es ist einfach zu übersehen.
            ->Salutation
        +   Sie sind sich nicht sicher wie dies funktioniert oder glauben es falsch gemacht zu haben? #CLASS:selected
            Kein Problem. Setzen Sie sich mit anderen hier auseinander, die dir helfen können. 
            -> Troubleshooter_help_links -> Salutation        
        +   Sie haben dies bereits versucht und suchen etwas anderes? #CLASS:selected
            Haben Sie (Valorant), Faceit oder HWMontior installiert?
            +   +   Ja! #CLASS:selected
                -> Windows_vanguard_faceit_HWMonitor
            +   +   Nein, keins von dem gelisteten. #CLASS:selected
                Setzen Sie sich mit anderen hier auseinander, die dir helfen können. 
                -> Troubleshooter_help_links -> Salutation
            +   +   Sie wissen nicht, was diese sind? #CLASS:selected
                Mit HWMonitor können Sie Sachen wie z.B. Temperaturen von Ihrem Computer auslesen.
                Vanguard ist das anti-cheat Programm von Riot, welches sich auf Kernel-Basis installiert.
                Die FaceIT software kommt mit ähnlicher Software.
                Diese Software spielt nicht gerne mit den von OpenRGB benötigten Treibern. 
                Dann kann ich Ihnen nicht weiterhelfen. Sie können das Problem hier mit anderen lösen. 
                -> Troubleshooter_help_links -> Salutation

= Espanol
    Esto es un problema corriente. Si has instalado la librería para acceder a la memoria, lo más probable es que otro programa esté bloqueando el acceso a la memoria. Si aún no has comprobado que los programas oficiales no estén corriendo, es probable que se ejecuten en segundo plano. Abre el administrador de tareas y asegúrate que ningún programa oficial se está ejecutando.  
    Asegúrate también de comprobar los servicios en la pestaña de servicios y mirando que iCue / Synapse / GHub aparezcan desactivados y detenidos. 
        +   ¡Fantástico! Eso ha solucionado mi problema. #CLASS:selected
            Perfecto, es un error fácil de cometer.
            ->Salutation
        +   No estoy muy seguro de como hacer eso, pero creo que no funcionó.  #CLASS:selected
            No hay problema, aunque quizá necesitas más ayuda de la que puedo ofrecer aquí.
            -> Troubleshooter_help_links -> Salutation        
        +   No, ya he probado eso. ¿Qué más puede ser? #CLASS:selected
            Con mucha probabilidad tendrás Valorant, Faceit or HWMonitor instalado. ¿Es eso cierto?
            +   +   ¡Efectivamente! #CLASS:selected
                -> Windows_vanguard_faceit_HWMonitor
            +   +   No, no tengo ninguno de esos programas instalados. #CLASS:selected
                Entonces lo siento, no puedo ayudarte más allá de aquí. 
                -> Troubleshooter_help_links -> Salutation
            +   +   Nunca he oído de estos programas. ¿Qué son? #CLASS:selected
                HWMonitor hace lo que dice su nombre, monitora tu hardware. Obviamente, para hacer eso tiene que tener acceso directo y próximo al hardware.
                Vanguard el programa anti fraude de Riot, que se instala en forma de driver de kernel.
                Similarmente, Faceit sirve el mismo propósito y se distribuye con la plataforma de e-sports Faceit. 
                Se trata de software disruptivo y con tendencia a interferir con otras aplicaciones del sistema. 
                Dicho esto, lamento decir que no puedo ayudar más allá de aquí. 
                -> Troubleshooter_help_links -> Salutation

= Russian
    Ух ты, знакомый случай. Если вы, как вы сказали, установили библиотеку-помощник для оперативной памяти, то скорее всего что-то мешает получить к ней доступ.
    Пожалуйста, убедитесь, что стандартная программа для управления оперативной памяти не висит где-нибудь в фоне. Откройте диспетчер задач (Ctrl+Shift+Esc) и проверьте, что процесс не запущен.
    Также стоить проверить, что сервисы iCue / Synapse / GHub остановлены и отключены.
        +   Это помогло, спасибо! #CLASS:selected
            Замечательно, такие вещи легко упустить.
            ->Salutation
        +   Я не знаю, что делать, или ничего не помогает #CLASS:selected
            Не страшно, вам просто нужна помощь a little more hand holding than I can offer you here.
            -> Troubleshooter_help_links -> Salutation
        +   No tried that. Is it something else? #CLASS:selected
            Похоже, у вас установлен Vanguard (Valorant), Faceit или HWMontior?
            +   +   Так точно! #CLASS:selected
                -> Windows_vanguard_faceit_HWMonitor
            +   +   Неа, ничего такого. #CLASS:selected
                Боюсь, вы нашли мою единственную слабость. С этим я не смогу помочь.
                -> Troubleshooter_help_links -> Salutation
            +   +   Это вообще кто? #CLASS:selected
                HWMonitor делает именно то, что сказано в названии - мониторит железо. Для этого ему приходится получать максимально прямой и полный доступ к оборудованию.
                Vanguard - это античит от Riot, который устанавливается как драйвер ядра.
                Платформа Faceit esports тоже поставляется с античитом.
                Все три программы не очень-то дружат с другими. Вообще не дружат. Отбирают все права на железо и не делятся.
                В общем, я тут, похоже, не смогу помочь.
                -> Troubleshooter_help_links -> Salutation
                


== Windows_vanguard_faceit_HWMonitor
{ language == "Français": -> Francais }  //Ink does not like non ascii characters in links
{ language == "Deutsch":  -> Deutsch  }
{ language == "Español":  -> Espanol  }  //Ink does not like non ascii characters in links
{ language == "Русский":  -> Russian  }  //Ink does not like non ascii characters in links
-> English  //Default stitch to display 

= English
~ temp hwmonitor = false
Right, I see and which of those is it?
    +   HWMonitor # CLASS:selected
        //set a temporary variable to true
        ~ hwmonitor = true     
        HWMonitor will take ownership of your RAM prior to OpenRGB being able to detect it. We recommend installing
        //Links need to be put on a seperate line to identify any translations required both before and after the link
        <> <a target="_blank" rel="noopener noreferrer" href="https:/<>/openhardwaremonitor.org/">Open Hardware Monitor</a>
        <> as an alternative.
    +   Vanguard and / or Faceit #CLASS:selected
        Vanguard and Faceit are anti-cheat software that believe they know better than you what software should and should not run on your computer. They are known to actively block the inpout32.dll library. If you feel that this is a mistake I would encourage you to file an issue / ticket with them to correct the problem. In the meantime, sadly you would have to choose between having Vanguard / Faceit and OpenRGB installed.
-   //This is a gather that will apply to both options above
    +   I will uninstall it right now! #CLASS:selected
        Great! Glad to hear that you've chosen open source and software freedom. I'm hoping that will resolve your issue.
        ->Salutation
    *   Sorry, I can't uninstall { hwmonitor == true: it | them}. I need { hwmonitor == true: it | them} in my life. #CLASS:selected
        That's okay! I know it's hard to choose your favorite child but we know you have one. Apologies that we couldn't assist further though. Thanks very much for considering us.
        -> END

= Francais
~ temp hwmonitor = false
Ok, je vois. Lequel de ces programme est installé ?
    +   HWMonitor # CLASS:selected
        //set a temporary variable to true
        ~ hwmonitor = true     
        HWMonitor s'approprie votre RAM avant qu'OpenRGB ne puisse la détecter. Nous vous recommandons d'installer
        //Links need to be put on a seperate line to identify any translations required both before and after the link
        <> <a target="_blank" rel="noopener noreferrer" href="https:/<>/openhardwaremonitor.org/">Open Hardware Monitor</a>
        <> comme alternative.
    +   Vanguard et / ou Faceit #CLASS:selected
        Vanguard et Faceit sont des logiciels anti-triche qui croient savoir mieux que vous quels logiciels doivent et ne doivent pas fonctionner sur votre ordinateur. Ils sont connus pour bloquer activement la bibliothèque inpout32.dll. Si vous pensez que c'est une erreur, je vous encourage à ouvrir un ticket avec eux pour corriger le problème. En attendant, vous devrez malheureusement choisir entre l'installation de Vanguard / Faceit et OpenRGB.
-   //This is a gather that will apply to both options above
    +   Je le désinstalle de suite ! #CLASS:selected
        Super! Heureux d'apprendre que vous avez choisi l'open source et la liberté logicielle. J'espère que cela résoudra votre problème.
        ->Salutation
    *   Désolé je ne veux pas { hwmonitor == true: le | les} désinstaller pour une bonne raison. #CLASS:selected
        Pas de problème! Je sais qu'il est difficile de choisir, mais nous respectons votre choix. Désolé, de ne pas avoir pu vous aider davantage. Merci beaucoup de nous avoir essayé.
        -> END

= Deutsch
~ temp hwmonitor = false
Welches von den gelisteten ist es?
    +   HWMonitor # CLASS:selected
        //set a temporary variable to true
        ~ hwmonitor = true     
        HWMonitor nimmt den Arbeitsspeicher ein und verhindert so Zugriff von OpenRGB. Wir empfehlen
        //Links need to be put on a seperate line to identify any translations required both before and after the link
        <> <a target="_blank" rel="noopener noreferrer" href="https:/<>/openhardwaremonitor.org/">Open Hardware Monitor</a>
        <> als Alternative.
    +   Vanguard und / oder Faceit #CLASS:selected
        Vanguard und Faceit sind anti-cheat Programme die glauben Ihren Computer besser zu kennen, wie Sie. Die sind bekannt dafür mutwillig die  inpout32.dll Bibliotek zu blockieren. Wir würden empfehlen, falls sie damit Schwerigkeiten empfinden mit den Machern von dem anti-cheat auseinanderzusetzen, damit diese das Problem lösen konnen. Zur Zeit müssen Sie sich leider entscheiden zwischen OpenRGB oder diesen anti-cheats.
-   //This is a gather that will apply to both options above
    +   Ich werde es deinstallieren! #CLASS:selected
        Großartig! Schön zu hören, dass Sie OpenSource und FOS bevorzugen. Wir hoffen, dass dies Ihr Problem lösen wird.
        ->Salutation
    *   Nein, dies kann ich nicht deinstallieren { hwmonitor == true: it | them}. Ich benötige { hwmonitor == true: it | them} diese Programme. #CLASS:selected
        Das ist OK! Es ist schwer etwas auszuwählen. Es tut uns leid, dass wir Ihnen nicht weiterhelfen können. Vielen Dank uns in Erwägung zu ziehen.
        -> END

= Espanol
~ temp hwmonitor = false
Ya veo. ¿Cual de esos programas tienes instalado?
    +   HWMonitor # CLASS:selected
        //set a temporary variable to true
        ~ hwmonitor = true   
        HWMonitor toma el control de tu RAM antes de que OpenRGB pueda detectarla. Recomendamos instalar
        //Links need to be put on a seperate line to identify any translations required both before and after the link
        <> <a target="_blank" rel="noopener noreferrer" href="https:/<>/openhardwaremonitor.org/">Open Hardware Monitor</a>
        <> como alternativa.
    +   Vanguard y/o Faceit #CLASS:selected
        Vanguard y Faceit son programas anti fraude que creen saber mejor que ti qué programas deberían correr en tu ordenador. Tenemos conocimiento de que bloquean la librería inpout32.dll. Si crees que esto es un error te animo a abrir una incidencia con los autores de este software para corregirlo. De mientras me temo que tendrás que escojer entre tener Vanguard/Faceit o OpenRGB instalado. 
-   //This is a gather that will apply to both options above
    +   ¡Voy a desinstalar{hwmonitor == true:lo |los} enseguida! #CLASS:selected
        ¡Fantástico! Me alegra que hayas escogido el código abierto y la libertad de software. Espero que esto resuelva tu problema.
        ->Salutation
    *   Lo siento, no puedo desinstalar{ hwmonitor == true:lo |los}. { hwmonitor == true: Lo | Los} necesito en mi vida. #CLASS:selected
       ¡No juzgamos! Sabemos que es difícil decidir, pero respetamos tu decisión. Lo siento, no puedo ayudarte a partir de aquí en adelante. Gracias por habernos probado.
       -> END

= Russian
~ temp hwmonitor = false
Ладно, понял, что именно нашлось?
    +   HWMonitor # CLASS:selected
        //set a temporary variable to true
        ~ hwmonitor = true
        HWMonitor захватывает доступ к RAM до того, как OpenRGB её находит. Советую вместо него установить
        //Links need to be put on a seperate line to identify any translations required both before and after the link
        <> <a target="_blank" rel="noopener noreferrer" href="https:/<>/openhardwaremonitor.org/">Open Hardware Monitor</a>.
    +   Vanguard и/или Faceit #CLASS:selected
        Vanguard и Faceit - это античит-программы, которые думают, что лучше вас знают, какое ПО можно или нельзя запускать на вашем компьютере. Известно, что они активно блокируют библиотеку inpout32.dll - ту самую, которую мы ставили пару шагов назад. Если вы считаете это ошибкой с их стороны, я бы посоветовал потребовать у разработчиков исправить эту проблему. Пока что, к сожалению, вам нужно выбрать - или Vanguard / Faceit, или OpenRGB.
-   //This is a gather that will apply to both options above
    +   Да я прямо сейчас их удалю! #CLASS:selected
        Отлично! Рад услышать, что вы выбрали Open Source и свободу ПО. Очень надеюсь, что это решит проблему.
        ->Salutation
    *   Увы, я не могу { hwmonitor == true: его | их} удалить. { hwmonitor == true: Он мне нужен | Они нужны мне}. #CLASS:selected
        Не страшно. I know it's hard to choose your favorite child but we know you have one. Apologies that we couldn't assist further though. Thanks very much for considering us.
        -> END
        


== Windows_device_not_present
{ language == "Français": -> Francais }  //Ink does not like non ascii characters in links
{ language == "Deutsch":  -> Deutsch  }
{ language == "Español":  -> Espanol  }  //Ink does not like non ascii characters in links
{ language == "Русский":  -> Russian  }  //Ink does not like non ascii characters in links
-> English  //Default stitch to display 

= English
    Right, so there is a device you are expecting to show up in OpenRGB but it is not appearing in the list of devices. Have you checked that the
    //Links need to be put on a seperate line to identify any translations required both before and after the link
    <> <a target="_blank" rel="noopener noreferrer" href="https:/<>/gitlab.com/CalcProgrammer1/OpenRGB/-/wikis/Supported-Devices">device is currently supported</a>
    <> by OpenRGB?
        +   Yes, my device is definitely in the supported list. # CLASS:selected
            -> Windows_device_supported_not_detected
        +   I didn't see my exact device model in the list but I think it is supported. # CLASS:selected
            -> Troubleshooter_unable_to_assist
        +   No, sadly I didn't see my device anywhere in the list. # CLASS:selected
            A shame for sure but that was the case for all devices at one stage. The path forward would be to read this article on Gitlab about how
            //Links need to be put on a seperate line to identify any translations required both before and after the link
            <> <a target="_blank" rel="noopener noreferrer" href="https:/<>/gitlab.com/CalcProgrammer1/OpenRGB/-/wikis/OpenRGB-doesn't-have-my-device">OpenRGB does not have my device</a>.
            -> Salutation

= Francais
    Bon, votre périphérique n'apparaît pas dans la liste alors que vous l'attendiez. Avez-vous vérifié qu'il
    //Links need to be put on a seperate line to identify any translations required both before and after the link
    <> <a target="_blank" rel="noopener noreferrer" href="https:/<>/gitlab.com/CalcProgrammer1/OpenRGB/-/wikis/Supported-Devices">est actuellement pris en charge</a>
    <> par OpenRGB?
        +   Oui mon périphérique est bien dans la liste de prise en charge. # CLASS:selected
            -> Windows_device_supported_not_detected
        +   Je ne vois pas le modèle exact dans la liste mais je pense qu'il est supporté. # CLASS:selected
            -> Troubleshooter_unable_to_assist
        +   Non, malheuresement je ne le vois pas dans la liste. # CLASS:selected
            Dommage ... mais c'était le cas pour tous les appareils à un moment donné. La voie à suivre est de lire cet article sur Gitlab expliquant pourquoi <a href="https:/<>/gitlab.com/CalcProgrammer1/OpenRGB/-/wikis/OpenRGB-doesn't-have-my-device">OpenRGB n'a pas mon périphérique</a>.
            -> Salutation

= Deutsch
    Sie haben gehofft, dass Ihr Gerät auftaucht. Es taucht jedoch nicht auf. Haben Sie überprüft,
    //Links need to be put on a seperate line to identify any translations required both before and after the link
    <> <a target="_blank" rel="noopener noreferrer" href="https:/<>/gitlab.com/CalcProgrammer1/OpenRGB/-/wikis/Supported-Devices">ob dieses Gerät von OpenRGB unterstützt</a>
    <> wird?
        +   Ja, das Gerät wird gelistet. # CLASS:selected
            -> Windows_device_supported_not_detected
        +   Ein ähnliches Gerät wird gelistet. # CLASS:selected
            -> Troubleshooter_unable_to_assist
        +   Nein, das Gerät wird nicht gelistet. # CLASS:selected
            Schade. Dies war ein Mal für alle Geräte hier. Lesen sie folgenden Artikel
            //Links need to be put on a seperate line to identify any translations required both before and after the link
            <> <a target="_blank" rel="noopener noreferrer" href="https:/<>/gitlab.com/CalcProgrammer1/OpenRGB/-/wikis/OpenRGB-doesn't-have-my-device">Artikel</a>.
            -> Salutation

= Espanol
    Bien, uno de tus dispositivos no aparece en la lista, aunque esperabas que fuera detectado. ¿Has comprobado que
    //Links need to be put on a seperate line to identify any translations required both before and after the link
    <> <a target="_blank" rel="noopener noreferrer" href="https:/<>/gitlab.com/CalcProgrammer1/OpenRGB/-/wikis/Supported-Devices"> el dispositivo sea compatible</a>
    <> con OpenRGB?
        +   Sí, mi dispositivo es compatible con OpenRGB. # CLASS:selected
            -> Windows_device_supported_not_detected
        +   No veo mi modelo exacto pero creo que es compatible. # CLASS:selected
            -> Troubleshooter_unable_to_assist
        +   No, el dispositivo no está en la lista. # CLASS:selected
            Lo sentimos, pero piensa que este también fue el caso para todos los dispositivos en algún momento. Tu próximo paso es leer este artículo para entender por qué
            //Links need to be put on a seperate line to identify any translations required both before and after the link
            <> <a target="_blank" rel="noopener noreferrer" href="https:/<>/gitlab.com/CalcProgrammer1/OpenRGB/-/wikis/OpenRGB-doesn't-have-my-device">OpenRGB no tiene tu dispositivo</a>.
            -> Salutation

= Russian
    Итак, у вас есть устройство, которое OpenRGB должен обнаружить, но оно не отображается в списке устройств. Проверяли ли вы, что оно есть в
    //Links need to be put on a seperate line to identify any translations required both before and after the link
    <> <a target="_blank" rel="noopener noreferrer" href="https:/<>/gitlab.com/CalcProgrammer1/OpenRGB/-/wikis/Supported-Devices">списке поддерживаемых устройств</a>?
        +   Да, оно определённо есть в списке # CLASS:selected
            -> Troubleshooter_unable_to_assist
        +   Моей конкретной модели там нет, но я уверен(а), что устройство поддерживается. # CLASS:selected
            -> Troubleshooter_unable_to_assist
        +   Увы, устройства в списке нет. # CLASS:selected
            Действительно, жаль, но так когда-то было со всеми устройствами. Первым шагом к тому, чтобы это исправить, стоит прочитать статью на Gitlab
            //Links need to be put on a seperate line to identify any translations required both before and after the link
            <> <a target="_blank" rel="noopener noreferrer" href="https:/<>/gitlab.com/CalcProgrammer1/OpenRGB/-/wikis/OpenRGB-doesn't-have-my-device">"OpenRGB не поддерживает моё устройство"</a>.
            -> Salutation



== Troubleshooter_unable_to_assist
//Send the troubleshooter here by default as a catch all until everything is written
{ language == "Français": -> Francais }  //Ink does not like non ascii characters in links
{ language == "Deutsch":  -> Deutsch  }
{ language == "Español":  -> Espanol  }  //Ink does not like non ascii characters in links
{ language == "Русский":  -> Russian  }  //Ink does not like non ascii characters in links
-> English  //Default stitch to display 

= English
    Oh, that is a shame as I am unable to assist with that right now.
    -> Troubleshooter_help_links -> Salutation

= Francais
    Je suis désolé de ne pas pouvoir vous aider d'avantage.
    -> Troubleshooter_help_links -> Salutation

= Deutsch
    Es tut uns Leid, Ihnen nicht weiter helfen zu können.
    -> Troubleshooter_help_links -> Salutation

= Espanol
    Lo siento, aún no puedo ayudarte con esto. 
    -> Troubleshooter_help_links -> Salutation
    
= Russian
    Очень жаль, но я пока не могу с этим помочь.
    -> Troubleshooter_help_links -> Salutation



== Troubleshooter_help_links
{ language == "Français": -> Francais }  //Ink does not like non ascii characters in links
{ language == "Deutsch":  -> Deutsch  }
{ language == "Español":  -> Espanol  }  //Ink does not like non ascii characters in links
{ language == "Русский":  -> Russian  }  //Ink does not like non ascii characters in links
-> English  //Default stitch to display 

= English
    If you use Discord then perhaps you could ask in the \#openrgb-tech-support channel on the
    //Links need to be put on a seperate line to identify any translations required both before and after the link
    <> <a target="_blank" rel="noopener noreferrer" href="https:/<>/discord.gg/AQwjJPY">OpenRGB server</a>.
    Alternatively there is the
    //Links need to be put on a seperate line to identify any translations required both before and after the link
    <> <a target="_blank" rel="noopener noreferrer" href="https:/<>/www.reddit.com/r/OpenRGB">OpenRGB Subreddit</a>.  
    Otherwise I would suggest creating a
    //Links need to be put on a seperate line to identify any translations required both before and after the link
    <> <a target="_blank" rel="noopener noreferrer" href="https:/<>/gitlab.com/CalcProgrammer1/OpenRGB/-/issues/new">new issue on GitLab</a>.
    ->->    //This is a tunnel and allows the calling text to continue 

= Francais
    Si vous utilisez Discord, vous pouvez demander dans le canal \#openrgb-tech-support sur le 
    //Links need to be put on a seperate line to identify any translations required both before and after the link
    <> <a target="_blank" rel="noopener noreferrer" href="https:/<>/discord.gg/AQwjJPY">Serveur OpenRGB</a>.
    Il existe également un
    //Links need to be put on a seperate line to identify any translations required both before and after the link
    <> <a target="_blank" rel="noopener noreferrer" href="https:/<>/www.reddit.com/r/OpenRGB">Subreddit OpenRGB</a>.
    Sinon, je vous propose de créer un
    //Links need to be put on a seperate line to identify any translations required both before and after the link
    <> <a target="_blank" rel="noopener noreferrer" href="https:/<>/gitlab.com/CalcProgrammer1/OpenRGB/-/issues/new"> nouveau ticket sur GitLab</a>.
    ->->    //This is a tunnel and allows the calling text to continue

= Deutsch
    Falls Sie Discord benutzen, können sie im \#openrgb-tech-support channel auf dem
    //Links need to be put on a seperate line to identify any translations required both before and after the link
    <> <a target="_blank" rel="noopener noreferrer" href="https:/<>/discord.gg/AQwjJPY">OpenRGB server</a> nachfragen.
    Alternativ gibt es auch ein
    //Links need to be put on a seperate line to identify any translations required both before and after the link
    <> <a target="_blank" rel="noopener noreferrer" href="https:/<>/www.reddit.com/r/OpenRGB">OpenRGB Subreddit</a>.  
    Sonst bleibt die
    //Links need to be put on a seperate line to identify any translations required both before and after the link
    <> <a target="_blank" rel="noopener noreferrer" href="https:/<>/gitlab.com/CalcProgrammer1/OpenRGB/-/issues/new">GitLab issue Seite</a>.
    ->->    //This is a tunnel and allows the calling text to continue 

= Espanol
    Si utilizas Discord puedes preguntar en el canal \#openrgb-tech-support en
    //Links need to be put on a seperate line to identify any translations required both before and after the link
    <> <a target="_blank" rel="noopener noreferrer" href="https:/<>/discord.gg/AQwjJPY">nuestro servidor</a>.
    Como alternativa puedes preguntar en
    //Links need to be put on a seperate line to identify any translations required both before and after the link
    <> <a target="_blank" rel="noopener noreferrer" href="https:/<>/www.reddit.com/r/OpenRGB">nuestro Subreddit</a>.
    Finalmente sugiero crear 
    //Links need to be put on a seperate line to identify any translations required both before and after the link
    <> <a target="_blank" rel="noopener noreferrer" href="https:/<>/gitlab.com/CalcProgrammer1/OpenRGB/-/issues/new">una nueva incidencia en nuestro repositorio GitLab</a>.
    ->->    //This is a tunnel and allows the calling text to continue 

= Russian
    Если вы пользуетесь Discord, возможно, вам помогут на канале \#openrgb-tech-support
    //Links need to be put on a seperate line to identify any translations required both before and after the link
    <> <a target="_blank" rel="noopener noreferrer" href="https:/<>/discord.gg/AQwjJPY">сервера OpenRGB</a>.
    А если вы пользуетесь Reddit, вам могут помочь на
    //Links need to be put on a seperate line to identify any translations required both before and after the link
            <> <a target="_blank" rel="noopener noreferrer" href="https:/<>/www.reddit.com/r/OpenRGB">сабреддите OpenRGB</a>.  
    В противном случае я могу вам предложить создать
    //Links need to be put on a seperate line to identify any translations required both before and after the link
    <> <a target="_blank" rel="noopener noreferrer" href="https:/<>/gitlab.com/CalcProgrammer1/OpenRGB/-/issues/new">заявку на GitLab</a>.
    ->->    //This is a tunnel and allows the calling text to continue 



== Salutation
{ language == "Français": -> Francais }  //Ink does not like non ascii characters in links
{ language == "Deutsch":  -> Deutsch  }
{ language == "Español":  -> Espanol  }  //Ink does not like non ascii characters in links
{ language == "Русский":  -> Russian  }  //Ink does not like non ascii characters in links
-> English  //Default stitch to display 

= English
Thanks very much for using the OpenRGB software. 
{ not Troubleshooter_unable_to_assist: <> Have Fun!}
    //The + here denotes that we can always select this option * can only be selected once
    *   K, thanks Bye! # CLASS:selected
        -> END
    +   Wait, wait, wait! I'd like to start over. # CLASS:selected
        -> Start

= Francais
Merci beaucoup d'utiliser le logiciel OpenRGB.
{ not Troubleshooter_unable_to_assist: <> S'amuser!}
    *   Au revoir ! À bientôt. # CLASS:selected
        -> END
    +   Attends attends ! J'aimerais recommencer. # CLASS:selected
        -> Start

= Deutsch
Vielen Dank für die Verwendung der OpenRGB-Software.
{ not Troubleshooter_unable_to_assist: <> Viel Spass!}
    *   Tschüss! # CLASS:selected
        -> END
    +   Warte warte warte! Ich würde gerne von vorne anfangen. # CLASS:selected
        -> Start

= Espanol
Muchas gracias por utilizar OpenRGB.
{ not Troubleshooter_unable_to_assist: <> ¡A disfrutar!}
    *   ¡Adiós! Nos vemos. # CLASS:selected
        -> END
    +   ¡Espera espera espera! Me gustaría empezar de nuevo. # CLASS:selected
        -> Start

= Russian
Спасибо, что пользуетесь OpenRGB!
    //The + here denotes that we can always select this option * can only be selected once
    *   Ага, спасибо за помощь! # CLASS:selected
        -> END
    +   Одну секунду! Я хочу начать с начала. # CLASS:selected
        -> Start
        
/*--------------------------------------------------------------------------------*\
| This is the template for adding new sections                                     |
| DO NOT EDIT THIS DIRECTLY - Take a copy and change that instead.                 |
\*--------------------------------------------------------------------------------*/
== Template
{ language == "Français": -> Francais }  //Ink does not like non ascii characters in links
{ language == "Deutsch":  -> Deutsch  }
{ language == "Español":  -> Espanol  }  //Ink does not like non ascii characters in links
{ language == "Русский":  -> Russian  }  //Ink does not like non ascii characters in links
-> English  //Default stitch to display 

= English
-> Salutation

= Francais
-> English  //Until the translation for this language is done divert to English

= Deutsch
-> English  //Until the translation for this language is done divert to English

= Espanol
-> English  //Until the translation for this language is done divert to English

= Russian
-> English  //Until the translation for this language is done divert to English

/*--------------------------------------------------------------------------------*\
| This is the template for adding new sections                                     |
| DO NOT EDIT THIS DIRECTLY - Take a copy and change that instead.                 |
\*--------------------------------------------------------------------------------*/
